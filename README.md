# docker-demo

Build demo application using GitLab CI and publish to container registry.

## Run using Docker

A Docker image is built automatically and pushed to the GitLab registry. You can run the application using `docker run`.

```bash
sudo docker run --env PORT=5000 -p 5000:5000 registry.gitlab.com/vilhelm/docker-demo
```
