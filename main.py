# gitlab.com/vilhelm/docker-demo

from flask import Flask, jsonify

app = Flask(__name__)

# error handlers
@app.errorhandler(404)
def error_404(e):
    return jsonify({"code": 404, "message": "page not found", "response": {}}), 404


# routes
@app.route("/")
def index():
    return jsonify(
        {
            "code": 200,
            "message": "request successful",
            "response": {"foobar": "hello world"},
        }
    )


if __name__ == "__main__":
    app.run(host="0.0.0.0")
