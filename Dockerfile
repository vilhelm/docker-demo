FROM tiangolo/meinheld-gunicorn:python3.7-alpine3.8

COPY requirements.txt .

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

COPY . /app
WORKDIR /app
